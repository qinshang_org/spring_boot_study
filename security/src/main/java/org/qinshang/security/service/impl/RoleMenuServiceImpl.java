package org.qinshang.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.qinshang.security.entity.sys.RoleMenu;
import org.qinshang.security.mapper.sys.RoleMenuMapper;
import org.qinshang.security.service.RoleMenuService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色菜单关联表 服务实现类
 * </p>
 *
 * @author Felix
 * @since 2019-03-22
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements RoleMenuService {

}
