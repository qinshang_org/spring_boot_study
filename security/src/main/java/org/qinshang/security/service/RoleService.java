package org.qinshang.security.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.qinshang.security.dto.IdsForm;
import org.qinshang.security.dto.sys.OperationDTO;
import org.qinshang.security.dto.sys.RoleDetailDto;
import org.qinshang.security.entity.sys.Role;
import org.qinshang.security.utils.DxGridPageUtil;
import org.qinshang.security.vo.sys.RoleVO;

/**
 * <p>
 * 系统角色表 服务类
 * </p>
 *
 * @author Felix
 * @since 2019-03-21
 */
public interface RoleService extends IService<Role> {
    Page<RoleVO> list(DxGridPageUtil dxGridPageUtil, String name);

    RoleDetailDto get(Long id);

    void enabledBatch(OperationDTO operation) throws Exception;

    void deleteBatch(IdsForm idsForm);

    void delete(Long id);

    void update(RoleDetailDto roleDetailDto) throws Exception;

    Long save(RoleDetailDto roleDetailDto)throws Exception ;
}
