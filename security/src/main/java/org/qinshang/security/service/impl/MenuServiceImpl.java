package org.qinshang.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.qinshang.security.dto.IdsForm;
import org.qinshang.security.dto.sys.PermissionDTO;
import org.qinshang.security.entity.sys.Menu;
import org.qinshang.security.mapper.sys.MenuMapper;
import org.qinshang.security.mapper.sys.RoleMenuMapper;
import org.qinshang.security.service.MenuService;
import org.qinshang.security.utils.Constant;
import org.qinshang.security.vo.sys.MenuVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * 系统菜单表 服务实现类
 * </p>
 *
 * @author Felix
 * @since 2019-03-21
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {

    @Autowired
    MenuMapper menuMapper;

    @Autowired
    RoleMenuMapper roleMenuMapper;

    @Override
    public List<MenuVO> getMenuVOTreeByRoot() {
        return menuMapper.getMenuVOTreeByRoot();
    }

    @Override
    public List<MenuVO> findByName(String name) {
        List<MenuVO> menuVos = menuMapper.getMenuVOTreeByName(name);
        return menuVos;
    }

    @Override
    public List<MenuVO> menuList(Long pid) {
        List<MenuVO> menuVos = null;
        if (StringUtils.isEmpty(pid)) {
            menuVos = menuMapper.getMenuVOTreeByRoot();
        } else {
            menuVos = menuMapper.getMenuVOTreeByPid(pid);
        }
        return menuVos;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Long save(PermissionDTO dto) throws Exception {
        //验证是否存在
        this.verify(dto.getId(), dto.getPermission());
        Menu menu = new Menu();
        BeanUtils.copyProperties(dto, menu);

        menuMapper.insert(menu);
        return menu.getId();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void update(PermissionDTO dto) throws Exception {
        //验证是否存在
        this.verify(dto.getId(), dto.getPermission());
        Menu menu = menuMapper.selectById(dto.getId());
        if (menu != null) {
            BeanUtils.copyProperties(dto, menu);
            menuMapper.updateById(menu);
        }else{
            throw new Exception(Constant.DATA_NOT_FOUND_MSG);
        }

    }

    /**
     * 1. 如果通过code查询不到菜单则可以新增
     * 2. 如果修改菜单，则传递过来的是 id 和 code
     * 2.1 如果不修改，则通过code查询到的id与传递过来的id一直，验证通过
     * 2.2 如果修改，则通过code查询不到结果，验证通过
     * 3. 如果新增菜单，则不传递id，判断code是否存在即可
     * <p>
     * 所以：要么查询不到，验证过，要么通过code查询到 id于传递一直 验证过
     *
     * @param id
     * @param permission
     * @throws Exception
     */
    private void verify(Long id, String permission) throws Exception {
        MenuVO menuVo = menuMapper.getMenuVOByPermission(permission);
        if (!(menuVo == null || menuVo.getId().equals(id))) {
            throw new Exception("该菜单编码已经存在！");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(Long id) throws Exception {
        MenuVO menuVO = menuMapper.getMenuVOTreeById(id);
        if (menuVO != null) {
            List<Long> ids = getIdsByMenuVO(menuVO);
            roleMenuMapper.deletePermissionByMenuIds(ids);
            menuMapper.deleteBatchIds(ids);
        }else{
            throw new Exception(Constant.DATA_NOT_FOUND_MSG);
        }

    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteInBatch(IdsForm idsForm) throws Exception {
        if (idsForm != null && !CollectionUtils.isEmpty(idsForm.getIds())) {
            List<MenuVO> menuVOList = menuMapper.getMenuVOTreeByIds(idsForm.getIds());
            List<Long> ids = getIdsByMenuVOList(menuVOList);
            roleMenuMapper.deletePermissionByMenuIds(ids);
            menuMapper.deleteBatchIds(ids);
        }
    }

    /**
     * 根据List<MenuVO> 递归获取菜单ID
     *
     * @param menuVOList
     * @return
     */
    private List<Long> getIdsByMenuVOList(List<MenuVO> menuVOList) {
        List<Long> ids = new LinkedList();
        if (!CollectionUtils.isEmpty(menuVOList)) {
            for (MenuVO menuVO : menuVOList) {
                ids.addAll(getIdsByMenuVO(menuVO));
            }
        }
        return ids;
    }

    /**
     * 根据MenuVO 递归获取菜单ID
     *
     * @param menuVO
     * @return
     */
    private List<Long> getIdsByMenuVO(MenuVO menuVO) {
        List<Long> ids = new LinkedList();
        if (menuVO != null) {
            ids.add(menuVO.getId());
            if (!CollectionUtils.isEmpty(menuVO.getChildren())) {
                ids.addAll(getIdsByMenuVOList(menuVO.getChildren()));
            }
        }
        return ids;
    }

    @Override
    public MenuVO findById(Long menuId) {
        MenuVO menuVO = menuMapper.getMenuVOTreeById(menuId);
        return menuVO;
    }

    @Override
    public Long findPid(Long id) {
        if (id == null) {
            return null;
        }
        MenuVO menuVO = menuMapper.getMenuVOTreeById(id);
        if (menuVO == null) {
            return null;
        }
        return menuVO.getPreId();
    }



    @Override
    public List<MenuVO> userMenuList(String userName) {
        return menuMapper.userMenuList(userName);
    }
}
