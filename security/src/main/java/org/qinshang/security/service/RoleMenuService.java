package org.qinshang.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.qinshang.security.entity.sys.RoleMenu;

/**
 * <p>
 * 角色菜单关联表 服务类
 * </p>
 *
 * @author Felix
 * @since 2019-03-22
 */
public interface RoleMenuService extends IService<RoleMenu> {

}
