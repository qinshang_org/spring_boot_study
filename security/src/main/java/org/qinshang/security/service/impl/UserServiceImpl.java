package org.qinshang.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.qinshang.security.dto.sys.OperationDTO;
import org.qinshang.security.dto.sys.UserDto;
import org.qinshang.security.dto.sys.UserEditDto;
import org.qinshang.security.entity.sys.User;
import org.qinshang.security.entity.sys.UserRole;
import org.qinshang.security.mapper.sys.UserDtoMapper;
import org.qinshang.security.mapper.sys.UserMapper;
import org.qinshang.security.mapper.sys.UserRoleMapper;
import org.qinshang.security.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 系统用户信息表 服务实现类
 * </p>
 *
 * @author Felix
 * @since 2019-03-21
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserDtoMapper, UserDto> implements UserService {

    @Autowired
    private UserDtoMapper userDtoMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRoleMapper userRoleMapper;

    @Override
    public Page<UserDto> listUser(Page page, String username) {
        return userDtoMapper.listUsers(page);
    }

    @Override
    public void save(UserEditDto dto) throws Exception{
        if (dto != null && dto.getUsername() != null) {
            User selectOne = userMapper.selectOne(new QueryWrapper<User>().eq("username", dto.getUsername()));
            if (selectOne != null) {
                throw new Exception("用户名已存在！");
            }
            User user = new User();
            BeanUtils.copyProperties(dto, user);
            user.setOrgId(dto.getOrgId());
            user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
            userMapper.insert(user);
            if (dto.getRoleIds() != null && dto.getRoleIds().size() > 0) {
                dto.getRoleIds().forEach(s -> {
                    UserRole userRole = new UserRole();
                    userRole.setUserId(user.getId());
                    userRole.setRoleId(Long.valueOf(s));
                    userRoleMapper.insert(userRole);
                });
            }
        }
    }

    @Override
    public void update(UserEditDto dto) {
        if (dto != null && dto.getId() != null) {
            User user = new User();
            BeanUtils.copyProperties(dto, user);
            user.setOrgId(dto.getOrgId());
            userMapper.updateById(user);
            QueryWrapper<UserRole> wrapper = new QueryWrapper<>();
            wrapper.eq("user_id", dto.getId());
            userRoleMapper.delete(wrapper);
            if (dto.getRoleIds() != null && dto.getRoleIds().size() > 0) {
                dto.getRoleIds().forEach(s -> {
                    UserRole userRole = new UserRole();
                    userRole.setUserId(user.getId());
                    userRole.setRoleId(Long.valueOf(s));
                    userRoleMapper.insert(userRole);
                });
            }
        }
    }

    @Override
    public UserEditDto get(Long id) {
        UserEditDto userEditDto = new UserEditDto();
        if (!StringUtils.isEmpty(id)) {
            User user = userMapper.selectById(id);
            if (user != null) {
                BeanUtils.copyProperties(user, userEditDto);
                UserRole userRole = new UserRole();
                userRole.setUserId(id);
                List<UserRole> userRoles = userRoleMapper.selectList(new QueryWrapper<>(userRole));
                userEditDto.setRoleIds(userRoles.stream().map(UserRole::getRoleId).collect(Collectors.toList()));
            }
        }
        return userEditDto;
    }

    @Override
    public void deleteBatch(List<Long> ids) {
        if (ids != null && ids.size() > 0) {
            ids.forEach(aLong -> {
                QueryWrapper<UserRole> wrapper = new QueryWrapper<>();
                wrapper.eq("user_id", aLong);
                userRoleMapper.delete(wrapper);
                userMapper.deleteById(aLong);
            });
        }
    }

    @Override
    public void enabledBatch(OperationDTO operation) {
        operation.getIds().forEach(aLong -> {
            User user = new User();
            user.setId(aLong);
            user.setEnabled(operation.getEnabled());
            userMapper.updateById(user);
        });
    }

    @Override
    public void updatePassword(UserEditDto dto) {
        User user = new User();
        user.setId(dto.getId());
        user.setPassword(new BCryptPasswordEncoder().encode(dto.getPassword()));
        userMapper.updateById(user);
    }
}
