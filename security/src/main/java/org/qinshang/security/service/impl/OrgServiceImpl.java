package org.qinshang.security.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.qinshang.security.dto.sys.OrgDTO;
import org.qinshang.security.dto.sys.OrgTreeDTO;
import org.qinshang.security.entity.sys.Org;
import org.qinshang.security.mapper.sys.OrgMapper;
import org.qinshang.security.service.OrgService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户组织机构表 服务实现类
 * </p>
 *
 * @author Felix
 * @since 2019-03-21
 */
@Service
public class OrgServiceImpl extends ServiceImpl<OrgMapper, Org> implements OrgService {

    @Autowired
    private OrgMapper orgMapper;

    @Override
    public Page<OrgDTO> listByParent(Page page, Long pid) {
        return orgMapper.listByParent(page, pid);
    }

    @Override
    public List<OrgTreeDTO> listTree(String name) {
        return orgMapper.listTree(name);
    }

    @Override
    public void save(OrgDTO dto) {
        if (dto != null) {
            Org org = new Org();
            BeanUtils.copyProperties(dto, org);
            orgMapper.insert(org);
        }
    }

    @Override
    public void update(OrgDTO dto) {
        if (dto != null && dto.getId() != null) {
            Org org = new Org();
            BeanUtils.copyProperties(dto, org);
            orgMapper.updateById(org);
        }
    }

    @Override
    public OrgDTO get(Long id) {
        OrgDTO orgDTO = new OrgDTO();
        if (id != null) {
            Org org = orgMapper.selectById(id);
            if (org != null) {
                BeanUtils.copyProperties(org, orgDTO);
            }
        }
        return orgDTO;
    }

    @Override
    public void deleteBatch(List<Long> ids) {
        if (ids != null && ids.size() > 0) {
            ids.forEach(aLong -> orgMapper.deleteById(aLong));
        }
    }
}
