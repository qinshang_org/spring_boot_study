package org.qinshang.security.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.qinshang.security.dto.IdsForm;
import org.qinshang.security.dto.sys.OperationDTO;
import org.qinshang.security.dto.sys.RoleDetailDto;
import org.qinshang.security.entity.sys.Role;
import org.qinshang.security.mapper.sys.RoleMapper;
import org.qinshang.security.mapper.sys.RoleMenuMapper;
import org.qinshang.security.mapper.sys.UserRoleMapper;
import org.qinshang.security.service.RoleService;
import org.qinshang.security.utils.CollectionUtils;
import org.qinshang.security.utils.Constant;
import org.qinshang.security.utils.DxGridPageUtil;
import org.qinshang.security.vo.sys.RoleVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统角色表 服务实现类
 * </p>
 *
 * @author Felix
 * @since 2019-03-21
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {
    @Autowired
    RoleMapper roleMapper;
    @Autowired
    RoleMenuMapper roleMenuMapper;

    @Autowired
    UserRoleMapper userRoleMapper;

    @Override
    public Page<RoleVO> list(DxGridPageUtil dxGridPageUtil, String name) {
        Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        Page<RoleVO> roleVOPage = roleMapper.selectRoleVOPage(dxGridPageUtil.pageable(), params);
        return roleVOPage;
    }


    @Override
    public RoleDetailDto get(Long id) {
        return roleMapper.getRoleDetailDtoById(id);
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void enabledBatch(OperationDTO operation) throws Exception {
        if (operation == null || CollectionUtils.isEmpty(operation.getIds())) {
            return;
        }
        if (operation.getEnabled() == null) {
            throw new Exception(String.valueOf(Constant.MISSING_PARAMETER_CODE));
        }
        roleMapper.batchUpdateEnabled(operation.getIds(), operation.getEnabled());
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteBatch(IdsForm idsForm) {
        if (idsForm == null || CollectionUtils.isEmpty(idsForm.getIds())) {
            return;
        }
        List<Long> ids = idsForm.getIds();
        roleMenuMapper.deletePermissionByRoleIds(ids);
        userRoleMapper.deleteRoleMenuByRoleIds(ids);
        roleMapper.deleteBatchIds(ids);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(Long id) {
        if (id == null) {
            return;
        }
        roleMenuMapper.deletePermissionByRoleId(id);
        userRoleMapper.deleteRoleMenuByRoleId(id);
        roleMapper.deleteById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void update(RoleDetailDto roleDetailDto) throws Exception {
        verify(roleDetailDto.getId(),roleDetailDto.getCode());
        if (StringUtils.isEmpty(roleDetailDto.getId())) {
            throw new Exception(Constant.MISSING_PARAMETER_MSG);
        }
        Role role = roleMapper.selectById(roleDetailDto.getId());
        if (role == null) {
            throw new Exception(Constant.CANNOT_FOUND_ERROR_MSG);
        }
        roleMenuMapper.deletePermissionByRoleId(roleDetailDto.getId());
        if (!CollectionUtils.isEmpty(roleDetailDto.getPermissionIds())) {
            roleMenuMapper.insertPermissions(roleDetailDto.getId(), roleDetailDto.getPermissionIds());
        }
        BeanUtils.copyProperties(roleDetailDto, role);
        roleMapper.updateById(role);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Long save(RoleDetailDto roleDetailDto)throws Exception {
        verify(roleDetailDto.getId(),roleDetailDto.getCode());
        Role role = new Role();
        BeanUtils.copyProperties(roleDetailDto, role);
        roleMapper.insert(role);
        if (!CollectionUtils.isEmpty(roleDetailDto.getPermissionIds())) {
            roleMenuMapper.insertPermissions(role.getId(), roleDetailDto.getPermissionIds());
        }
        return role.getId();
    }

    /**
     * 1. 如果通过code查询不到菜单则可以新增
     * 2. 如果修改菜单，则传递过来的是 id 和 code
     * 2.1 如果不修改，则通过code查询到的id与传递过来的id一直，验证通过
     * 2.2 如果修改，则通过code查询不到结果，验证通过
     * 3. 如果新增菜单，则不传递id，判断code是否存在即可
     * <p>
     * 所以：要么查询不到，验证过，要么通过code查询到 id于传递一直 验证过
     *
     * @param id
     * @param code
     * @throws Exception
     */
    private void verify(Long id, String code) throws Exception {
        RoleVO roleVO = roleMapper.getMenuVOByCode(code);
        if (!(roleVO == null || roleVO.getId().equals(id))) {
            throw new Exception("该角色编码已经存在！");
        }
    }
}
