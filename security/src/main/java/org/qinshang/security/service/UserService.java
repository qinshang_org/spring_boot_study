package org.qinshang.security.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.qinshang.security.dto.sys.OperationDTO;
import org.qinshang.security.dto.sys.UserDto;
import org.qinshang.security.dto.sys.UserEditDto;

import java.util.List;

/**
 * <p>
 * 系统用户信息表 服务类
 * </p>
 *
 * @author Felix
 * @since 2019-03-21
 */
public interface UserService extends IService<UserDto> {

    /**
     * 用户列表
     * @param page
     * @param keyword 搜索关键字
     * @return
     */
    Page<UserDto> listUser(Page page, String keyword);

    /**
     * 保存用户
     * @param dto 页面参数
     * @return
     */
    void save(UserEditDto dto) throws Exception;

    /**
     * 修改用户信息
     * @param dto 页面参数
     * @return
     */
    void update(UserEditDto dto);

    /**
     * 查询详细信息
     * @param id 主键ID
     * @return
     */
    UserEditDto get(Long id);

    /**
     * 批量删除
     * @param ids 主键list
     */
    void deleteBatch(List<Long> ids);

    /**
     * 启用、停用操作
     * @param operation 页面参数
     */
    void enabledBatch(OperationDTO operation);

    /**
     * 重置密码
     * @param dto 页面参数
     */
    void updatePassword(UserEditDto dto);
}
