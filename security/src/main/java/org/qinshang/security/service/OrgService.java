package org.qinshang.security.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.qinshang.security.dto.sys.OrgDTO;
import org.qinshang.security.dto.sys.OrgTreeDTO;
import org.qinshang.security.entity.sys.Org;

import java.util.List;

/**
 * <p>
 * 用户组织机构表 服务类
 * </p>
 *
 * @author Felix
 * @since 2019-03-21
 */
public interface OrgService extends IService<Org> {

    /**
     * 根据父节点分页查询
     * @param page
     * @param pid 父节点ID
     * @return
     */
    Page listByParent(Page page, Long pid);

    /**
     * 获取组织结构树
     * @param name 节点名称
     * @return
     */
    List<OrgTreeDTO> listTree(String name);

    /**
     * 保存
     * @param dto 页面参数
     */
    void save(OrgDTO dto);

    /**
     * 修改
     * @param dto 页面参数
     */
    void update(OrgDTO dto);

    /**
     * 查询详细信息
     * @param id 主键
     * @return
     */
    OrgDTO get(Long id);

    /**
     * 批量删除
     * @param ids
     */
    void deleteBatch(List<Long> ids);
}
