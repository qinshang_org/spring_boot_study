package org.qinshang.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.qinshang.security.dto.IdsForm;
import org.qinshang.security.dto.sys.PermissionDTO;
import org.qinshang.security.entity.sys.Menu;
import org.qinshang.security.vo.sys.MenuVO;

import java.util.List;

/**
 * <p>
 * 系统菜单表 服务类
 * </p>
 *
 * @author Felix
 * @since 2019-03-21
 */
public interface MenuService extends IService<Menu> {
    List<MenuVO> getMenuVOTreeByRoot();

    List<MenuVO> menuList(Long pid);

    List<MenuVO> findByName(String name);

    Long save(PermissionDTO permissionDTO)throws Exception;

    void update(PermissionDTO dto) throws Exception;

    void delete(Long id)throws Exception;

    void deleteInBatch(IdsForm idsForm)throws Exception;

    MenuVO findById(Long menuId);

    Long findPid(Long id);

    List<MenuVO> userMenuList(String userName);

}
