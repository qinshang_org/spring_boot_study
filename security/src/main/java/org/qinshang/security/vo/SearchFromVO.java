package org.qinshang.security.vo;

import org.qinshang.security.utils.DateUtils;

import java.io.Serializable;

public class SearchFromVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String startDate;
    private String endDate;
    private String idName;
    private String idKind;
    private String idNo;
    private String boardTrainCode;
    private String fromTeleCode;
    private String toTeleCode;
    private String ticketNo;
    private String coachNo;
    private String seatNo;

    public SearchFromVO() {
        super();
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = DateUtils.strToStr_yyyyMMdd (startDate);
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate =  DateUtils.strToStr_yyyyMMdd (endDate);
    }

    public String getIdName() {
        return idName;
    }

    public void setIdName(String idName) {
        this.idName = idName;
    }

    public String getIdKind() {
        return idKind;
    }

    public void setIdKind(String idKind) {
        this.idKind = idKind;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getBoardTrainCode() {
        return boardTrainCode;
    }

    public void setBoardTrainCode(String boardTrainCode) {
        this.boardTrainCode = boardTrainCode;
    }

    public String getFromTeleCode() {
        return fromTeleCode;
    }

    public void setFromTeleCode(String fromTeleCode) {
        this.fromTeleCode = fromTeleCode;
    }

    public String getToTeleCode() {
        return toTeleCode;
    }

    public void setToTeleCode(String toTeleCode) {
        this.toTeleCode = toTeleCode;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getCoachNo() {
        return coachNo;
    }

    public void setCoachNo(String coachNo) {
        this.coachNo = coachNo;
    }

    public String getSeatNo() {
        return seatNo;
    }

    public void setSeatNo(String seatNo) {
        this.seatNo = seatNo;
    }
}
