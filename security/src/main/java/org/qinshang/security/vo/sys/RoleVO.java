package org.qinshang.security.vo.sys;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class RoleVO {

	private Long id;

	/**
	 * 角色名称
	 */
	private String name;

	/**
	 * 角色编码
	 */
	private String code;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 是否启用
	 */
	private Boolean enabled;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}


	public RoleVO(Long id, String name, String code, Date createTime, String remark, Boolean enabled) {
		super();
		this.id = id;
		this.name = name;
		this.code = code;
		this.createTime = createTime;
		this.remark = remark;
		this.enabled = enabled;
	}

	public RoleVO() {
		super();
	}


	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}
}
