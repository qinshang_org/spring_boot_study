package org.qinshang.security.vo.sys;

import org.qinshang.security.entity.BaseEntity;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 系统菜单表
 * </p>
 *
 * @author Felix
 * @since 2019-03-21
 */
public class MenuVO extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;
    /**
     * 权限编码
     */
    private String permission;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 权限名
     */
    private String name;
    /**
     * 权限类型
     */
    private Integer type;
    /**
     * 请求地址
     */
    private String url;
    /**
     * 图标
     */
    private String icon;
    /**
     * 上级id
     */
    private Long preId;

    private List<MenuVO> children;

    public List<MenuVO> getChildren() {
        return children;
    }

    public void setChildren(List<MenuVO> children) {
        this.children = children;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Long getPreId() {
        return preId;
    }

    public void setPreId(Long preId) {
        this.preId = preId;
    }
}
