package org.qinshang.security.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.qinshang.security.utils.Constant;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Felix
 * @date 2019/3/22
 */
public class AuthExceptionEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException) throws IOException, ServletException {
        System.out.println("*************************************");
        System.out.println("异常类型: " + authException.getClass());
        System.out.println("异常信息: " + authException.getMessage());
        System.out.println("*************************************");

        Map<String, Object> map = new HashMap<>();
        String errorMsg = authException.getMessage();

        int code;
        String message;

        if (authException instanceof InsufficientAuthenticationException) {
            code = Constant.BAD_TOKEN_CODE;
            message = Constant.BAD_TOKEN_MSG;
        } else if (errorMsg.contains("Full authentication is required to access this resource")) {
            code = Constant.UNAUTHORIZED_CODE;
            message = Constant.UNAUTHORIZED_MSG;
        } else if (errorMsg.contains("Access token expired")) {
            code = Constant.TOKEN_EXPIRED_CODE;
            message = Constant.TOKEN_EXPIRED_MSG;
        } else if (errorMsg.contains("Cannot convert access token to JSON")) {
            code = Constant.BAD_TOKEN_CODE;
            message = Constant.BAD_TOKEN_MSG;
        } else {
            code = Constant.UNKNOW_ERROR_CODE;
            message = Constant.UNKNOW_ERROR_MSG;
        }

        map.put("code", code);
        map.put("message", message);
        map.put("data", null);

        response.setContentType("application/json");
        response.setStatus(code);

        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(response.getOutputStream(), map);
        } catch (Exception e) {
            throw new ServletException();
        }
    }

}
