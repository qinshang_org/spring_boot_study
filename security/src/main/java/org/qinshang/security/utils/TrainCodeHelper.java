package org.qinshang.security.utils;

import java.util.HashMap;
import java.util.Map;

public abstract class TrainCodeHelper {
	public static Map<String, String> seatTypeMapName = new HashMap<String, String>();
	public static Map<String, String> idKindMapName = new HashMap<String, String>();
	public static Map<String, String> ticketTypeMapName = new HashMap<String, String>();

	static {
		seatTypeMapName.put("0", "棚车");
		seatTypeMapName.put("1", "硬座");
		seatTypeMapName.put("2", "软座");
		seatTypeMapName.put("3", "硬卧");
		seatTypeMapName.put("4", "软卧");
		seatTypeMapName.put("5", "包厢硬卧");
		seatTypeMapName.put("6", "高级软卧");
		seatTypeMapName.put("7", "一等软座");
		seatTypeMapName.put("8", "二等软座");
		seatTypeMapName.put("9", "商务座");
		seatTypeMapName.put("A", "鸳鸯软卧");
		seatTypeMapName.put("B", "混编硬座");
		seatTypeMapName.put("C", "混编硬卧");
		seatTypeMapName.put("D", "包厢软座");
		seatTypeMapName.put("E", "特等软座");
		seatTypeMapName.put("F", "四人软包");
		seatTypeMapName.put("G", "二人软包");
		seatTypeMapName.put("H", "一人软包");
		seatTypeMapName.put("I", "一等双软");
		seatTypeMapName.put("J", "二等双软");
		seatTypeMapName.put("K", "混编软座");
		seatTypeMapName.put("L", "混编软卧");
		seatTypeMapName.put("M", "一等座");
		seatTypeMapName.put("O", "二等座");
		seatTypeMapName.put("P", "特等座");
		seatTypeMapName.put("Q", "观光座");
		seatTypeMapName.put("S", "一等包座 ");
		seatTypeMapName.put("WZ", "无座");

		idKindMapName.put("1", "二代身份证");
		idKindMapName.put("2", "一代身份证");
		idKindMapName.put("3", "临时");
		idKindMapName.put("4", "武警");
		idKindMapName.put("5", "军官");
		idKindMapName.put("6", "军士");
		idKindMapName.put("7", "军学");
		idKindMapName.put("8", "军文");
		idKindMapName.put("9", "军退");
		idKindMapName.put("B", "护照");
		idKindMapName.put("E", "内港");
		idKindMapName.put("F", "内台");
		idKindMapName.put("G", "台内");
		idKindMapName.put("H", "外留");
		idKindMapName.put("I", "外入");
		idKindMapName.put("J", "外官");
		idKindMapName.put("K", "领馆");
		idKindMapName.put("L", "海员");
		idKindMapName.put("M", "学生");
		idKindMapName.put("N", "户口");
		idKindMapName.put("O", "免票");
		idKindMapName.put("Q", "驾驶");
		idKindMapName.put("R", "暂居");
		idKindMapName.put("S", "结婚");

		idKindMapName.put("T", "保障");
		idKindMapName.put("U", "医保");
		idKindMapName.put("A", "军保");
		idKindMapName.put("P", "释放");
		idKindMapName.put("D", "救助");
		idKindMapName.put("C", "港内");

		/**
		 * 1 全 2 孩 3 学 4 残 5 免 6 探 7 半 8 单卧小孩 9 团 10 军 11 红色旅游  12 返 13 卧 14 集 31
		 * 令 15 农
		 */
		ticketTypeMapName.put("1", "成人票");
		ticketTypeMapName.put("2", "儿童票");
		ticketTypeMapName.put("3", "学生票");
		ticketTypeMapName.put("4", "残军票");
		ticketTypeMapName.put("5", "公免票");
		ticketTypeMapName.put("6", "探亲票");
		ticketTypeMapName.put("7", "半价票");
		ticketTypeMapName.put("8", "单卧小孩票");
		ticketTypeMapName.put("9", "团优票");
		ticketTypeMapName.put("10", "军人票");
		ticketTypeMapName.put("11", "红色旅游票");
		ticketTypeMapName.put("12", "学返票");
		ticketTypeMapName.put("13", "卧");
		ticketTypeMapName.put("14", "集");
		ticketTypeMapName.put("15", "农");
		ticketTypeMapName.put("31", "令");


	};

}
