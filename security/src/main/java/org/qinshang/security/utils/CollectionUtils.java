package org.qinshang.security.utils;

import java.util.Collection;

/**
 * 集合工具雷
 * @author qindingtao
 *
 */
public class CollectionUtils {
	
	
	/**
	 * 判断集合是否为空或者集合内是否有数据
	 * @param collection
	 * @return
	 */
	public static Boolean isEmpty(Collection collection) {
		return collection==null||collection.size()==0;
	}

}
