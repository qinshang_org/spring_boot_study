package org.qinshang.security.utils;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @author Felix
 * @date 2019/3/22
 */
public class DxGridPageUtil {

    /**
     * 条数
     */
    private int pageSize = 10;

    /**
     * 页数
     */
    private int pageNum = 1;

    /**
     * 分组
     */
    private String orderBy;


    /**
     * 排序
     */
    private String sort;

    private Integer posStart;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        pageNum = this.getPosStart() / this.getPageSize();
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public String getOrderBy() {
        return orderBy;
    }


    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public Integer getPosStart() {
        if (posStart == null) {
            posStart = 0;
        }
        return posStart;
    }

    public void setPosStart(Integer posStart) {
        this.posStart = posStart;
    }

    public Page pageable() {
        return new Page<>(getPageNum(), getPageSize());
    }

}
