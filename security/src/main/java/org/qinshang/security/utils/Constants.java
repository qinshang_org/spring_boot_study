package org.qinshang.security.utils;

import java.util.HashMap;
import java.util.Map;

public abstract class Constants {
    public static String SUPERADMIN = "11010819730922471X"; // d13zGjmy
    /*超级管理员*/
    public static String SUPERLEVEL = "1";
    /*路局管理员*/
    public static String TRAINLEVEL = "2"; 
    /*普通用户*/
    public static String GENERALLEVEL = "3"; 

    /* 一级菜单 */
    public static Integer menu_1_level = 1;

    /* 二级菜单 */
    public static Integer menu_2_level = 2;

    /* 用户维护状态，1为启用 */
    public static Integer User_status_on = 1;

    /* 用户维护状态，-1为停用 */
    public static Integer User_status_off = -1;
    
    /*秦皇岛*/
    public static String QINGHUANGDAO = "1";

    public static final String ORDER_SEQUENCENO_INDEX = "order_sequenceno_index";
    public static final String ORDER_INFO_INDEX = "order_info_index";
    public static final String ORDER_INFO = "order_info";
    public static final String OFFICE_NO = "office_no";
    public static final String TICKETNO_INDEX = "ticketno_index";
    public static final String USERINFO_INDEX = "userinfo_index";
    public static final String SALE_RECORD = "sale_record";
    public static final String SALE_RECORD_INDEX = "sale_record_index";
    public static final String CANCEL_RECORD = "cancel_record";
    public static final String RETURN_RECORD = "return_record";
    public static final String RESIGN_RECORD = "resign_record";
    public static final String CLUSTER_NAME = "ots";
    public static final String KEYSPACE_NAME = "user_train";
    public static final String STATION_INDEX = "station_index";
    
    public static final String XINJIANG_WEIZU = "xj_wz_idno";
    
    public static int MAX_RANGE = Integer.MAX_VALUE;
    
    public static Map<String ,String> OFFICENO_MAP = new HashMap<String,String>() ;
    public static int OFFICENO_SIZE = 0 ;
    
    public static String LINK = "#";

    /** 
     * 存根类型
     * 1为正常
     * */
    public static String RECORD_NORMAL = "1";
    /**
     * 存根类型
     * 2为废票
     */
    public static String RECORD_CANCLE = "2";
    /**
     * 存根类型
     * 3为退票
     */
    public static String RECORD_RETURN = "3";
    /**
     * 存根类型
     * 4为改签 
     */
    public static String RECORD_RESIGN = "4";
    /**
     * ID_KIND_ONE:一代身份证
     */
    public static String ID_KIND_ONE = "2";
    /**
     * ID_KIND_SECOND:二代身份证
     */
    public static String ID_KIND_SECOND= "1";
    /**
     * ID_KIND_Three临时身份证
     */
    public static String ID_KIND_Three = "3";
    /**
     * B:护照
     */
    public static String B = "B";
    /**
     * C：港澳通行证
     */
    public static String C = "C";
    
    /**
     * G：台湾
     */
    public static String G = "G";
    
    /**
     * 表示12306用户名
     */
    public static String LOGIN_NAME = "1";
    /**
     * 表示订票人证件号
     */
    public static String RESERVER_ID_NO = "2";
    /**
     * 用于大量数据比对时，对数据进行分割，每次查询2100条记录。
     * 20130524 每次查询限制1500 条 updateby 刘阳
     * ONCE_QUERY_COUNT：表示一次查询的数量
     */
    public static int ONCE_QUERY_COUNT=1500;
    public static String[] TRAINRECORD_COLUMNS = new String[] { "key", "id_kind", "id_name",
            "id_no", "train_date", "board_train_code", "start_time",
            "from_tele_code", "to_tele_code", "from_station_name",
            "to_station_name", "operater_no", "record_type" };
    /**
     * SUPPER_CODE：用来表示铁道部的局码，用于权限控制。
     * A:表示铁道部用户
     */
    public static String SUPPER_CODE="A";
}
