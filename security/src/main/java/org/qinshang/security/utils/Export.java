package org.qinshang.security.utils;

import net.sf.jxls.transformer.XLSTransformer;
import org.apache.poi.ss.usermodel.Workbook;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;

public class Export {

   /* 导出excel的模板*/
    public static  void export(String excelName, String fileName, Object entity, HttpSession session, HttpServletResponse response) {

        try {
            String xlsx=".xlsx";
            File file = new File("src/main/resources/excel/"+excelName+".xlsx");
            response.reset();// 必要地清除response中缓存的信息
            response.setHeader("Content-disposition",
                    "attachment;filename=" + new String((fileName+xlsx).getBytes("GBK"), "iso-8859-1"));
            InputStream is =new FileInputStream (file);
            XLSTransformer former = new XLSTransformer();
            HashMap<String, Object> beanParams = new HashMap<>();
            beanParams.put("entity", entity);
            Workbook workBook = former.transformXLS(is, beanParams);
            workBook.write(response.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
