package org.qinshang.security.utils;


import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.PropertyFilter;
import com.alibaba.fastjson.serializer.SerializeWriter;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.fasterxml.jackson.core.type.TypeReference;
import org.qinshang.security.exception.EsRuntimeException;

import java.util.HashSet;
import java.util.Set;

public abstract class JsonUtils {

    public static String toJson(Object jsonObject, String... filterNames) {
        try {
            return PojoMapper.toJson(jsonObject, filterNames);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T toObject(String text, Class<T> type) {
        try {
            return PojoMapper.fromJson(text, type);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    // 可转换List等，调用方式：toObject(str,new TypeReference<List<QueryDTO>>(){});
    public static <T> T toObject(String text, TypeReference<T> type) {
        try {
            return PojoMapper.fromJson(text, type);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public static String toJsonImport(Object jsonObject, String... filterNames) {
		SerializeWriter out = new SerializeWriter();

		try {
			JSONSerializer serializer = new JSONSerializer(out);
			serializer.config(SerializerFeature.DisableCircularReferenceDetect, true);
			serializer.config(SerializerFeature.WriteDateUseDateFormat, true);
			serializer.setDateFormat("yyyy-MM-dd HH:mm:ss");
			if (filterNames.length > 0) {
				final Set<String> filters = new HashSet<String>();
				for (String filterName : filterNames) {
					filters.add(filterName);
				}
				serializer.getPropertyFilters().add(new PropertyFilter() {
					@Override
					public boolean apply(Object source, String name, Object value) {
						return !filters.contains(name);
					}
				});
			}
			serializer.write(jsonObject);
			return serializer.toString();
		} catch (StackOverflowError e) {
			throw new EsRuntimeException("maybe circular references", e);
		} finally {
			out.close();
		}
	}

}
