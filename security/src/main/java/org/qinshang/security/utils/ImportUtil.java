package org.qinshang.security.utils;

import java.io.*;
import java.util.HashSet;
import java.util.Set;


public class ImportUtil {

//	private static final String PATH = "D:\\test.txt";

	public static final void readFiles(String filePath) throws IOException {
		FileReader fileReader = new FileReader(filePath);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String string;
		while ((string = bufferedReader.readLine().trim()) != null) {
			if (0 != string.length()) {
				System.out.println(string);
			}
		}
		fileReader.close();
	}
	
	public static String getFileNameSuffix(String filename) {
		return filename.substring(filename.lastIndexOf('.'), filename.length())
				.toLowerCase();
	}
	
	/**
	 * 解析导入的文件，将来可以扩展
	 * 
	 * 
	 * @author eva.liu
	 * @param uploadFile
	 * @return
	 * @throws IOException
	 */
	public static Set<String> parseImportedFile(InputStream input)
			throws Exception {
		BufferedReader br = null;
		Set<String> id_noSet = new HashSet<String>();
		try {
			br = new BufferedReader(new InputStreamReader(input));
			for (String line = br.readLine(); line != null; line = br
					.readLine()) {
				if (line != null && line.trim().length() > 0) {
					id_noSet.add(line.trim());// 去掉首尾空格
				}
			}
		} catch (Exception e) {
			throw new Exception("解析文件出错");
		} finally {
			try {
				if (br != null){
					br.close();
				}
			} catch (IOException e) {
				// avoid this
			}
		}
		return id_noSet;
	}


}
