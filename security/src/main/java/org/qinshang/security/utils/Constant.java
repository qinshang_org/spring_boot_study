package org.qinshang.security.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Felix
 * @date 2019/3/22
 */
public class Constant {

    public static final String PROJECT_PC_CLIENT = "client";

    public static final String PROJECT_PC_SECRET = "secret";

    /**
     * 系统管理
     */
    public static final String SYSTEM = "system";

    /**
     * 不拦截的路径
     */
    public static final List<String> IGNORE_PATHS;

    static {
        IGNORE_PATHS = new ArrayList<>();
        // swagger-ui
        IGNORE_PATHS.add("/swagger-ui.html");
        IGNORE_PATHS.add("/swagger/**");
        IGNORE_PATHS.add("/webjars/**");
        IGNORE_PATHS.add("/swagger-resources/**");
        IGNORE_PATHS.add("/v1/**");
        IGNORE_PATHS.add("/v2/**");
        //
    }

    /**
     * 未传token;client的id和secret错误;访问无权限的web接口
     */
    public static final int UNAUTHORIZED_CODE = 401;

    /**
     * 服务器内部错误
     */
    public static final int INTERNAL_ERROR_CODE = 500;

    /**
     * 未知异常
     */
    public static final int UNKNOW_ERROR_CODE = 600;

    /**
     * 访问登陆接口, 未写用户名密码或者用户名密码不匹配
     */
    public static final int USERNAME_OR_PASSWORD_ERROR_CODE = 601;

    /**
     * 缺少grant_type
     */
    public static final int MISSING_GRANT_TYPE_CODE = 602;

    /**
     * token过期
     */
    public static final int TOKEN_EXPIRED_CODE = 603;

    /**
     * token解析错误
     */
    public static final int BAD_TOKEN_CODE = 604;

    /**
     * refresh_token过期
     */
    public static final int REFRESH_TOKEN_EXPIRED_CODE = 605;

    /**
     * 缺少参数
     */
    public static final int MISSING_PARAMETER_CODE = 606;

    /**
     * 参数类型不匹配
     */
    public static final int ARG_TYPE_MISMATCH_CODE = 607;

    /**
     * 404异常
     */
    public static final int CANNOT_FOUND_ERROR_CODE = 404;

    /**
     * 方法不被支持(如请求方式get, post)
     */
    public static final int METHOD_NOT_SUPPORTED_CODE = 405;

    public static final String UNAUTHORIZED_MSG = "缺少凭据";

    public static final String INTERNAL_ERROR_MSG = "服务器内部错误";

    public static final String USERNAME_OR_PASSWORD_ERROR_MSG = "用户名或密码错误";

    public static final String OLD_PASSWORD_ERROR_MSG = "旧密码错误";

    public static final String METHOD_NOT_SUPPORTED_MSG = "请求方式错误";

    public static final String MISSING_GRANT_TYPE_MSG = "缺少授权方式";

    public static final String UNKNOW_ERROR_MSG = "未知异常";

    public static final String TOKEN_EXPIRED_MSG = "token已过期";

    public static final String BAD_TOKEN_MSG = "无效的token";

    public static final String REFRESH_TOKEN_EXPIRED_MSG = "refresh_token已过期";

    public static final String CANNOT_FOUND_ERROR_MSG = "资源无法找到";

    public static final String MISSING_PARAMETER_MSG = "缺少参数";

    public static final String ARG_TYPE_MISMATCH_MSG = "参数类型错误";

    public static final String DATA_NOT_FOUND_MSG = "数据不存在";

    public static final String EXISTS_RELATION_NOT_DELETE_MSG = "该数据存在关联关系数据，不允许删除";
}
