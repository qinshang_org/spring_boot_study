package org.qinshang.security.utils;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

/**
 * @author felix.ma
 * @create 2018-11-12 16:45
 **/
public class PinYinUtil {

    public static String getPinYin(String str) {
        StringBuilder py = null;
        try {
            HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
            format.setCaseType(HanyuPinyinCaseType.UPPERCASE);
            format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
            py = new StringBuilder();
            String temp = "";
            String[] t;
            for (int i = 0; i < str.length(); i++) {
                char c = str.charAt(i);
                if ((int) c <= 128) {
                    py.append(c);
                } else {
                    t = PinyinHelper.toHanyuPinyinStringArray(c, format);
                    if (t == null) {
                        py.append(c);
                    } else {
                        temp = t[0];
                        if (temp.length() >= 1) {
                            temp = temp.substring(0, 1);
                        }
                        py.append(temp);
                    }
                }
            }
            return py.toString().trim();
        } catch (BadHanyuPinyinOutputFormatCombination badHanyuPinyinOutputFormatCombination) {
            badHanyuPinyinOutputFormatCombination.printStackTrace();
            return str;
        }
    }
}
