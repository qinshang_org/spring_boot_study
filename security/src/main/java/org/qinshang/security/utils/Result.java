package org.qinshang.security.utils;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Result {

    private static final String SUCCESS_MSG = "success";

    private static final String ERROR_MSG = "error";

    private Object data;

    private int code;

    private String message;

    public static ResponseEntity<Result> custom(int code, String msg, Object data) {
        Result r = new Result();
        r.setCode(code);
        r.setMessage(msg);
        r.setData(data);
        return new ResponseEntity<Result>(r, HttpStatus.valueOf(code));
    }

    public static ResponseEntity<Result> info(int code) {
        return info(code, null, null);
    }

    public static ResponseEntity<Result> info(int code, Object data) {
        return info(code, data, null);
    }

    public static ResponseEntity<Result> info(int code, Object data, String message) {
        Result r = new Result();
        r.setCode(code);
        r.setMessage(message);
        r.setData(data);
        return new ResponseEntity<Result>(r, HttpStatus.OK);
    }

    public static ResponseEntity<Result> ok() {
        return ok(null, null);
    }

    public static ResponseEntity<Result> ok(Object data) {
        return ok(data, null);
    }

    public static ResponseEntity<Result> ok(Object data, String message) {
        Result r = new Result();
        r.setCode(HttpStatus.OK.value());
        r.setMessage(message);
        r.setData(data);
        return new ResponseEntity<Result>(r, HttpStatus.OK);
    }

    public static ResponseEntity<Result> error() {
        return error(null);
    }

    public static ResponseEntity<Result> error(Object data) {
        return error(data, null);
    }

    public static ResponseEntity<Result> error(Object data, String message) {
        Result r = new Result();
        r.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        r.setMessage(message);
        r.setData(data);
        return new ResponseEntity<Result>(r, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static ResponseEntity<Result> page(Page page) {
        Result r = new Result();
        r.setCode(HttpStatus.OK.value());
        r.setMessage(SUCCESS_MSG);
        DhtmlxGrid grid = new DhtmlxGrid();
        grid.setPos((page.getCurrent() - 1) * page.getSize());
        grid.setTotal_count(page.getTotal());
        grid.setContent(page.getRecords());
        r.setData(grid);
        return new ResponseEntity<Result>(r, HttpStatus.OK);
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
