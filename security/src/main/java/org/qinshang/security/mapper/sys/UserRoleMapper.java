package org.qinshang.security.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.qinshang.security.entity.sys.UserRole;

import java.util.List;

/**
 * 用户角色关联
 *
 * @author zhangqiang
 * @since 2019-03-22
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {
    void deleteRoleMenuByRoleId(@Param("id") Long id);

    void deleteRoleMenuByRoleIds(@Param("ids") List<Long> ids);
}
