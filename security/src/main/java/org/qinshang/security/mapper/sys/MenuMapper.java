package org.qinshang.security.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.qinshang.security.entity.sys.Menu;
import org.qinshang.security.vo.sys.MenuVO;

import java.util.List;

/**
 * <p>
 * 系统菜单表 Mapper 接口
 * </p>
 *
 * @author Felix
 * @since 2019-03-21
 */
public interface MenuMapper extends BaseMapper<Menu> {
    /**
     * 获取树形菜单
     * @return
     */
    List<MenuVO> getMenuVOTreeByRoot();

    /**
     * 获取树形菜单
     * @return
     */
    List<MenuVO> getMenuVOTreeByPid(@Param("pid") Long pid);
    /**
     * 获取树形菜单
     * @return
     */
    List<MenuVO> getMenuVOTreeByName(@Param("name") String name);
    /**
     * 获取非树形菜单
     * @return
     */
    MenuVO getMenuVOByPermission(@Param("permission") String permission);

    MenuVO getMenuVOTreeById(@Param("id") Long id);

    List<MenuVO> getMenuVOTreeByIds(@Param("ids") List<Long> ids);

    List<MenuVO> userMenuList(@Param("userName") String userName);
}
