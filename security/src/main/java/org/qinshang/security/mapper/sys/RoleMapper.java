package org.qinshang.security.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.qinshang.security.dto.sys.RoleDetailDto;
import org.qinshang.security.entity.sys.Role;
import org.qinshang.security.vo.sys.RoleVO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统角色表 Mapper 接口
 * </p>
 *
 * @author Felix
 * @since 2019-03-21
 */
public interface RoleMapper extends BaseMapper<Role> {
    Page<RoleVO> selectRoleVOPage(Page page, @Param("params") Map<String, Object> params);

    RoleDetailDto getRoleDetailDtoById(@Param("id") Long id);

    void batchUpdateEnabled(@Param("ids") List<Long> ids, @Param("enabled") Boolean enabled);

    RoleVO getMenuVOByCode(@Param("code") String code);
}
