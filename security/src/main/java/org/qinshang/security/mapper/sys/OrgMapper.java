package org.qinshang.security.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.qinshang.security.dto.sys.OrgDTO;
import org.qinshang.security.dto.sys.OrgTreeDTO;
import org.qinshang.security.entity.sys.Org;

import java.util.List;

/**
 * <p>
 * 用户组织机构表 Mapper 接口
 * </p>
 *
 * @author Felix
 * @since 2019-03-21
 */
public interface OrgMapper extends BaseMapper<Org> {

    /**
     * 根据父节点分页查询
     * @param page
     * @param pid 父节点ID
     * @return
     */
    Page<OrgDTO> listByParent(Page page, @Param("pid") Long pid);

    /**
     * 机构树
     * @param name
     * @return
     */
    List<OrgTreeDTO> listTree(@Param("name") String name);
}
