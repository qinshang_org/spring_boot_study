package org.qinshang.security.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.qinshang.security.entity.sys.User;

/**
 * @author Felix
 * @date 2019/3/22
 **/
public interface UserMapper extends BaseMapper<User> {
}
