package org.qinshang.security.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.qinshang.security.entity.OauthClientDetails;

/**
 * @author Felix
 * @date 2019/3/15
 */
public interface OauthClientDetailsMapper extends BaseMapper<OauthClientDetails> {

}
