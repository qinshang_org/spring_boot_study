package org.qinshang.security.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.qinshang.security.entity.sys.RoleMenu;

import java.util.List;

/**
 * <p>
 * 角色菜单关联表 Mapper 接口
 * </p>
 *
 * @author Felix
 * @since 2019-03-22
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
    void deletePermissionByRoleIds(@Param("roleIds") List<Long> roleIds);

    void deletePermissionByRoleId(@Param("id") Long id);

    void insertPermissions(@Param("roleId") Long roleId, @Param("menuIds") List<Long> menuIds);

    void deletePermissionByMenuIds(@Param("ids") List<Long> ids);

    void deletePermissionByMenuId(@Param("id") Long id);

}
