package org.qinshang.security.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.qinshang.security.dto.sys.UserDto;

/**
 * <p>
 * 系统用户信息表 Mapper 接口
 * </p>
 *
 * @author Felix
 * @since 2019-03-21
 */
public interface UserDtoMapper extends BaseMapper<UserDto> {

    /**
     * 用过用户名查询当前登录用户
     *
     * @param username
     * @return
     */
    UserDto findByUsername(String username);

    /**
     * 分页查询用户
     *
     * @param page
     * @return
     */
    Page<UserDto> listUsers(Page page);

}
