package org.qinshang.security.config;

import org.qinshang.security.dto.sys.UserDto;
import org.qinshang.security.mapper.sys.UserDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @author Felix
 * @date 2019/3/15
 */
public class UserSecurityImpl implements UserDetailsService {

    @Autowired
    UserDtoMapper userDtoMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDto user = userDtoMapper.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Can not found user: " + username);
        }
        return user;
    }

}
