package org.qinshang.security.dto.sys;

import java.io.Serializable;
import java.util.List;

/**
 * 用户管理 - 接收页面参数
 *
 * @author zhangqiang
 * @date 2019年3月23日 12:20:40
 */
public class OperationDTO implements Serializable {

    private List<Long> ids;

    private Boolean enabled;

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "OperationDTO{" +
                "ids=" + ids +
                ", enabled=" + enabled +
                '}';
    }
}
