package org.qinshang.security.dto.sys;

import com.alibaba.fastjson.JSON;
import org.qinshang.security.dto.BaseDto;
import org.qinshang.security.entity.sys.Menu;

import java.util.List;

/**
 * @author Felix
 * @date 2019/3/21
 **/
public class RoleDto extends BaseDto {

    private Long id;
    /**
     * 角色编码
     */
    private String code;
    /**
     * 角色名
     */
    private String name;
    /**
     * 组织机构id
     */
    private Long orgId;

    /**
     * 菜单
     */
    private List<Menu> menus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
