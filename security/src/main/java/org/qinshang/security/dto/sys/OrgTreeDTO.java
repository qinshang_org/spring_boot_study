package org.qinshang.security.dto.sys;

import java.util.List;

/**
 * @author zhangqiang
 * @Description: 组织机构树
 * @date 2019/3/23 15:57
 */
public class OrgTreeDTO {

    /**
     * id
     */
    private Long id;
    /**
     * 上级id
     */
    private Long preId;
    /**
     * 组织机构名称
     */
    private String name;
    /**
     * 组织机构代码
     */
    private String code;
    /**
     * 级别
     */
    private String level;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 负责人
     */
    private String head;
    /**
     * 组织机构类型
     */
    private Integer type;

    /**
     * 子节点
     */
    private List<OrgTreeDTO> children;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPreId() {
        return preId;
    }

    public void setPreId(Long preId) {
        this.preId = preId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<OrgTreeDTO> getChildren() {
        return children;
    }

    public void setChildren(List<OrgTreeDTO> children) {
        this.children = children;
    }
}
