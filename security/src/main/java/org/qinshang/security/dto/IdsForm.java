package org.qinshang.security.dto;

import java.util.List;

public class IdsForm implements  java.io.Serializable{

	private List<Long> ids;

	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}
}
