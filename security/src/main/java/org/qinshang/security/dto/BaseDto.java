package org.qinshang.security.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Felix
 * @date 2019/3/21
 **/
public class BaseDto implements Serializable {
    /**
     * 创建时间
     */
    protected Date createTime;
    /**
     * 更新时间
     */
    protected Date updateTime;

    private void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    private void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
