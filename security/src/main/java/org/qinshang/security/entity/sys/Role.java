package org.qinshang.security.entity.sys;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.TableName;
import org.qinshang.security.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 * 系统角色表
 * </p>
 *
 * @author Felix
 * @since 2019-03-21
 */
@TableName("sys_role")
public class Role extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 角色编码
     */
    private String code;
    /**
     * 角色名
     */
    private String name;
    /**
     * 组织机构id
     */
    private Long orgId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否启用
     */
    private Boolean enabled;

    private Long dataPermissionId;


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
