package org.qinshang.security.entity.sys;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.TableName;
import org.qinshang.security.entity.BaseEntity;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 角色菜单关联表
 * </p>
 *
 * @author Felix
 * @since 2019-03-22
 */
@TableName("sys_role_menu")
public class RoleMenu extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色id
     */
    private Long roleId;
    /**
     * 权限id
     */
    private Long menuId;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }



    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }}