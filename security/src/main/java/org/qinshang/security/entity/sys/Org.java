package org.qinshang.security.entity.sys;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户组织机构表
 * </p>
 *
 * @author zhangqiang
 * @since 2019-03-23
 */
@TableName("sys_org")
public class Org implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;
    /**
     * 上级id
     */
    private Long preId;
    /**
     * 组织机构名称
     */
    private String name;
    /**
     * 组织机构代码
     */
    private String code;
    /**
     * 级别
     */
    private String level;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 负责人
     */
    private String head;
    /**
     * 组织机构类型
     */
    private Integer type;
    /**
     * 传真
     */
    private String fax;
    /**
     * 电话
     */
    private String phone;
    /**
     * 备注
     */
    private String remark;
    /**
     * 地址
     */
    private String address;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPreId() {
        return preId;
    }

    public void setPreId(Long preId) {
        this.preId = preId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "Org{" +
        ", id=" + id +
        ", preId=" + preId +
        ", name=" + name +
        ", code=" + code +
        ", level=" + level +
        ", sort=" + sort +
        ", head=" + head +
        ", type=" + type +
        ", fax=" + fax +
        ", phone=" + phone +
        ", remark=" + remark +
        ", address=" + address +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
