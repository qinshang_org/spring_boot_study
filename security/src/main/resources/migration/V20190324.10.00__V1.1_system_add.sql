ALTER TABLE "public"."sys_user"
  ADD COLUMN "remark" varchar(500);

COMMENT ON COLUMN "public"."sys_user"."remark" IS '备注';