/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     2019/3/22 14:11:43                           */
/*==============================================================*/


drop table if exists public.oauth_access_token;
drop table if exists public.oauth_client_details;
drop table if exists public.oauth_refresh_token;
drop table if exists public.sys_data_permission;
drop table if exists public.sys_data_relation;
drop table if exists public.sys_menu;
drop table if exists public.sys_org;
drop table if exists public.sys_role;
drop table if exists public.sys_role_menu;
drop table if exists public.sys_user;
drop table if exists public.sys_user_role;

/*==============================================================*/
/* User: public                                                 */
/*==============================================================*/
/*==============================================================*/
/* Table: oauth_access_token                                    */
/*==============================================================*/
create table public.oauth_access_token (
   create_time          DATE                 null default CURRENT_DATE,
   token_id             VARCHAR(255)         null,
   token                BYTEA                null,
   authentication_id    VARCHAR(255)         null,
   user_name            VARCHAR(255)         null,
   client_id            VARCHAR(255)         null,
   authentication       BYTEA                null,
   refresh_token        VARCHAR(255)         null
)
without oids;

comment on table public.oauth_access_token is
'用户认证token 可存储在redis中';

comment on column oauth_access_token.create_time is
'token创建时间';

comment on column oauth_access_token.token_id is
'token唯一值';

comment on column oauth_access_token.token is
'token';

comment on column oauth_access_token.authentication_id is
'权限id';

comment on column oauth_access_token.user_name is
'用户名';

comment on column oauth_access_token.client_id is
'客户端id';

comment on column oauth_access_token.authentication is
'权限内容';

comment on column oauth_access_token.refresh_token is
'refersh_token的外键';

/*==============================================================*/
/* Table: oauth_client_details                                  */
/*==============================================================*/
create table public.oauth_client_details (
   client_id                VARCHAR(255)         not null,
   resource_ids             VARCHAR(255)         null,
   client_secret            VARCHAR(255)         null,
   scope                    VARCHAR(255)         null,
   authorized_grant_types   VARCHAR(255)         null,
   web_server_redirect_uri  VARCHAR(255)         null,
   authorities              VARCHAR(255)         null,
   access_token_validity    INT8                 null,
   refresh_token_validity   INT8                 null,
   additional_information   VARCHAR(255)         null,
   autoapprove              VARCHAR(255)         null,
   constraint PK_OAUTH_CLIENT_DETAILS primary key (client_id)
)
without oids;

comment on table public.oauth_client_details is
'应用服务认证信息';

comment on column oauth_client_details.client_id is
'客户端id';

comment on column oauth_client_details.resource_ids is
'资源id';

comment on column oauth_client_details.client_secret is
'客户端密钥';

comment on column oauth_client_details.scope is
'范围';

comment on column oauth_client_details.authorized_grant_types is
'授权方式';

comment on column oauth_client_details.web_server_redirect_uri is
'授权回调地址';

comment on column oauth_client_details.authorities is
'权限';

comment on column oauth_client_details.access_token_validity is
'token失效时间，单位：秒';

comment on column oauth_client_details.refresh_token_validity is
'refresh_token失效时间，单位：秒';

comment on column oauth_client_details.additional_information is
'附加信息';

comment on column oauth_client_details.autoapprove is
'授权的一个选项，集成第三方登录的时候才用的到';

/*==============================================================*/
/* Table: oauth_refresh_token                                   */
/*==============================================================*/
create table public.oauth_refresh_token (
   create_time          DATE                 null default CURRENT_DATE,
   token_id             VARCHAR(255)         null,
   token                BYTEA                null,
   authentication       BYTEA                null
)
without oids;

comment on table public.oauth_refresh_token is
'用户刷新token 可存储在redis中';

comment on column oauth_refresh_token.create_time is
'token创建时间';

comment on column oauth_refresh_token.token_id is
'token唯一键';

comment on column oauth_refresh_token.token is
'token值';

comment on column oauth_refresh_token.authentication is
'认证信息';

/*==============================================================*/
/* Table: sys_data_permission                                   */
/*==============================================================*/
create table public.sys_data_permission (
   id                   INT8                 not null,
   label                VARCHAR(255)         null,
   create_time          TIMESTAMP            null default CURRENT_TIMESTAMP,
   update_time          TIMESTAMP            null default CURRENT_TIMESTAMP,
   type                 VARCHAR(255)         null,
   constraint PK_SYS_DATA_PERMISSION primary key (id)
);

comment on table public.sys_data_permission is
'数据表字段权限控制';

comment on column sys_data_permission.id is
'id';

comment on column sys_data_permission.label is
'数据权限别名';

comment on column sys_data_permission.create_time is
'创建时间';

comment on column sys_data_permission.update_time is
'更新时间';

comment on column sys_data_permission.type is
'类型code';

/*==============================================================*/
/* Table: sys_data_relation                                     */
/*==============================================================*/
create table public.sys_data_relation (
   id                   INT8                 not null,
   key                  VARCHAR(255)         null,
   code                 VARCHAR(255)         null,
   des                  VARCHAR(255)         null,
   create_time          TIMESTAMP            null default CURRENT_TIMESTAMP,
   update_time          TIMESTAMP            null default CURRENT_TIMESTAMP,
   constraint PK_SYS_DATA_RELATION primary key (id)
);

comment on table public.sys_data_relation is
'数据权限关联';

comment on column sys_data_relation.id is
'id';

comment on column sys_data_relation.key is
'数据库字段';

comment on column sys_data_relation.code is
'数据编码';

comment on column sys_data_relation.des is
'字段说明';

comment on column sys_data_relation.create_time is
'创建时间';

comment on column sys_data_relation.update_time is
'更新时间';

/*==============================================================*/
/* Table: sys_menu                                              */
/*==============================================================*/
create table public.sys_menu (
   id                   INT8                 not null,
   pre_id               INT8                 null,
   name                 VARCHAR(255)         null,
   type                 INT4                 null,
   sort                 INT4                 null,
   permission           VARCHAR(255)         not null,
   url                  VARCHAR(255)         null,
   icon                 VARCHAR(255)         null,
   create_time          TIMESTAMP            null default CURRENT_TIMESTAMP,
   update_time          TIMESTAMP            null default CURRENT_TIMESTAMP,
   constraint PK_SYS_MENU primary key (id)
)
without oids;

comment on table public.sys_menu is
'系统菜单表';

comment on column sys_menu.id is
'id';

comment on column sys_menu.pre_id is
'上级id';

comment on column sys_menu.name is
'权限名';

comment on column sys_menu.type is
'权限类型';

comment on column sys_menu.sort is
'排序';

comment on column sys_menu.permission is
'权限编码';

comment on column sys_menu.url is
'请求地址';

comment on column sys_menu.icon is
'图标';

comment on column sys_menu.create_time is
'创建时间';

comment on column sys_menu.update_time is
'更新时间';

/*==============================================================*/
/* Table: sys_org                                               */
/*==============================================================*/
create table public.sys_org (
   id                   INT8                 not null,
   pre_id               INT8                 null,
   name                 VARCHAR(255)         null,
   code                 VARCHAR(255)         not null,
   level                VARCHAR(255)         null,
   sort                 INT4                 null,
   head                 VARCHAR(255)         null,
   type                 INT4                 null,
   fax                  VARCHAR(255)         null,
   phone                VARCHAR(255)         null,
   remark               VARCHAR(255)         null,
   address              VARCHAR(255)         null,
   create_time          TIMESTAMP            null default CURRENT_TIMESTAMP,
   update_time          TIMESTAMP            null default CURRENT_TIMESTAMP,
   constraint PK_SYS_ORG primary key (id)
);

comment on table public.sys_org is
'用户组织机构表';

comment on column sys_org.id is
'id';

comment on column sys_org.pre_id is
'上级id';

comment on column sys_org.name is
'组织机构名称';

comment on column sys_org.code is
'组织机构代码';

comment on column sys_org.level is
'级别';

comment on column sys_org.sort is
'排序';

comment on column sys_org.head is
'负责人';

comment on column sys_org.type is
'组织机构类型';

comment on column sys_org.fax is
'传真';

comment on column sys_org.phone is
'电话';

comment on column sys_org.remark is
'备注';

comment on column sys_org.address is
'地址';

comment on column sys_org.create_time is
'创建时间';

comment on column sys_org.update_time is
'更新时间';

/*==============================================================*/
/* Table: sys_role                                              */
/*==============================================================*/
create table public.sys_role (
   id                   INT8                 not null,
   code                 VARCHAR(255)         not null,
   name                 VARCHAR(255)         null,
   org_id               INT8                 null,
   data_permission_id   INT8                 null,
   enabled              BOOL                 null,
   remark               VARCHAR(255)         null,
   create_time          TIMESTAMP            null default CURRENT_TIMESTAMP,
   update_time          TIMESTAMP            null default CURRENT_TIMESTAMP,
   constraint PK_SYS_ROLE primary key (id)
)
without oids;

comment on table public.sys_role is
'系统角色表';

comment on column sys_role.code is
'角色编码';

comment on column sys_role.name is
'角色名';

comment on column sys_role.org_id is
'组织机构id';

comment on column sys_role.data_permission_id is
'数据权限';

comment on column sys_role.enabled is
'是否启用';

comment on column sys_role.remark is
'备注';

/*==============================================================*/
/* Table: sys_role_menu                                         */
/*==============================================================*/
create table public.sys_role_menu (
   role_id              INT8                 not null,
   menu_id              INT8                 not null,
   create_time          TIMESTAMP            null default CURRENT_TIMESTAMP,
   update_time          TIMESTAMP            null default CURRENT_TIMESTAMP
)
without oids;

comment on table public.sys_role_menu is
'角色菜单关联表';

comment on column sys_role_menu.role_id is
'角色id';

comment on column sys_role_menu.menu_id is
'权限id';

comment on column sys_role_menu.create_time is
'创建时间';

comment on column sys_role_menu.update_time is
'更新时间';

/*==============================================================*/
/* Table: sys_user                                              */
/*==============================================================*/
create table public.sys_user (
   id                   INT8                 not null,
   org_id               VARCHAR(255)         null,
   username             VARCHAR(255)         not null,
   password             VARCHAR(255)         null,
   enabled              BOOL                 null,
   create_time          TIMESTAMP            null default CURRENT_TIMESTAMP,
   update_time          TIMESTAMP            null default CURRENT_TIMESTAMP,
   real_name            VARCHAR(255)         null,
   phone                VARCHAR(255)         null,
   email                VARCHAR(255)         null,
   sex                  VARCHAR(1)           null,
   id_card              VARCHAR(18)          null,
   constraint PK_SYS_USER primary key (id)
)
without oids;

comment on table public.sys_user is
'系统用户信息表';

comment on column sys_user.id is
'id';

comment on column sys_user.org_id is
'组织机构id';

comment on column sys_user.username is
'用户名';

comment on column sys_user.password is
'密码';

comment on column sys_user.enabled is
'是否启用';

comment on column sys_user.create_time is
'创建时间';

comment on column sys_user.update_time is
'更新时间';

comment on column sys_user.real_name is
'中文姓名';

comment on column sys_user.phone is
'手机号';

comment on column sys_user.email is
'邮箱';

comment on column sys_user.sex is
'性别';

comment on column sys_user.id_card is
'身份证号码';

/*==============================================================*/
/* Table: sys_user_role                                         */
/*==============================================================*/
create table public.sys_user_role (
   user_id              INT8                 not null,
   role_id              INT8                 not null,
   create_time          TIMESTAMP            null default CURRENT_TIMESTAMP,
   update_time          TIMESTAMP            null default CURRENT_TIMESTAMP
)
without oids;

comment on table public.sys_user_role is
'用户角色表';

comment on column sys_user_role.user_id is
'用户id';

comment on column sys_user_role.role_id is
'角色id';

comment on column sys_user_role.create_time is
'创建时间';

comment on column sys_user_role.update_time is
'更新时间';


-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO "public"."sys_user_role" VALUES (1, 1, '2019-03-22 09:24:55.731126', '2019-03-22 09:24:55.731126');
INSERT INTO "public"."sys_user_role" VALUES (1, 2, '2019-03-22 09:25:00.426874', '2019-03-22 09:25:00.426874');

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO "public"."sys_user"("id", "org_id", "username", "password", "enabled", "create_time", "update_time", "real_name", "phone", "email", "sex", "id_card") VALUES (1, '1', 'admin', '$2a$10$rRvoxKc/Uu0hW3gqqwKT0ugCPtEU1hAByGrli8BlGAh9dxDhcR00C', 't', '2019-03-22 14:16:31.628189', '2019-03-22 14:16:31.628189', '超级管理员', NULL, NULL, NULL, NULL);


-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO "public"."sys_role"("id", "code", "name", "org_id", "data_permission_id", "enabled", "remark", "create_time", "update_time") VALUES (1, 'admin', 'admin', 1, NULL, 't', NULL, '2019-03-22 14:17:59.716874', '2019-03-22 14:17:59.716874');
INSERT INTO "public"."sys_role"("id", "code", "name", "org_id", "data_permission_id", "enabled", "remark", "create_time", "update_time") VALUES (2, 'manager', 'manager', 1, NULL, 't', NULL, '2019-03-22 14:18:12.256057', '2019-03-22 14:18:12.256057');


-- ----------------------------
-- Records of sys_org
-- ----------------------------
INSERT INTO "public"."sys_org"("id", "pre_id", "name", "code", "level", "sort", "head", "type", "fax", "phone", "remark", "address", "create_time", "update_time") VALUES (1, NULL, 'org', 'org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-22 14:18:49.802433', '2019-03-22 14:18:49.802433');


-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO "public"."sys_menu"("id", "pre_id", "name", "type", "sort", "permission", "url", "icon", "create_time", "update_time") VALUES (1, NULL, '系统管理', 0, 1, 'sys', NULL, NULL, '2019-03-22 14:19:29.300439', '2019-03-22 14:19:29.300439');
INSERT INTO "public"."sys_menu"("id", "pre_id", "name", "type", "sort", "permission", "url", "icon", "create_time", "update_time") VALUES (2, 1, '系统管理查询', 1, 1, 'sys:query', NULL, NULL, '2019-03-22 14:19:45.728652', '2019-03-22 14:19:45.728652');
INSERT INTO "public"."sys_menu"("id", "pre_id", "name", "type", "sort", "permission", "url", "icon", "create_time", "update_time") VALUES (3, 1, '系统管理新增', 1, 2, 'sys:insert', NULL, NULL, '2019-03-22 14:20:01.397923', '2019-03-22 14:20:01.397923');

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO "public"."sys_role_menu" VALUES (1, 1, '2019-03-22 09:30:31.910024', '2019-03-22 09:30:31.910024');
INSERT INTO "public"."sys_role_menu" VALUES (1, 2, '2019-03-22 09:30:33.680402', '2019-03-22 09:30:33.680402');
INSERT INTO "public"."sys_role_menu" VALUES (1, 3, '2019-03-22 09:30:35.108255', '2019-03-22 09:30:35.108255');
INSERT INTO "public"."sys_role_menu" VALUES (2, 1, '2019-03-22 09:30:36.349372', '2019-03-22 09:30:36.349372');

-- ----------------------------
-- Records of oauth_client_details
-- ----------------------------
INSERT INTO "public"."oauth_client_details" VALUES ('client', NULL, '$2a$10$WpTMTFqRra4nXk8h1JEsS.w4OCEXgDTyP6tDUNAv/nsI.zq0iIW6S', 'read,write', 'password,refresh_token', NULL, NULL, NULL, NULL, NULL, '*');